angular.module('MyModule',[])


    .run(function ($templateCache) {
            $templateCache.put('hello.html','<div>Hi everyone!!!</div>')
    })
    .directive('hello',function ($templateCache) {
        return{
            restrict:'AEMC',//分别代表四种模式 attribute element comment class 前两种常用
            template:$templateCache.get('hello.html'),

            //template:'<div>Hi everyone!!!</div>',
            replace:true
        }

    })

    .directive('transclude',function () {
        return{
            restrict:'AE',
            transclude:true,
            template:'<div>Hi everyone!!!<div ng-transclude></div></div>'
            link:function (scope,element,attrs) {
                element.addClass('btn btn-primary');
            }
        }

    })

    .controller('MyCtrl1',['$scope',function ($scope) {
        $scope.loadDate = function () {
            console.log('加载数据方式1111');
        }
    }])

    .controller('MyCtrl2',['$scope',function ($scope) {
        $scope.loadDate2 = function () {
            console.log('加载数据方式2222');
        }
    }])

    .directive('loader',function () {
        return{
            restrict:'AE',
            link:function (scope,element,attrs) {
                element.bind('mouseenter',function (event) {
                    //注意这里的坑，howToLoad会被转换成howtoload
                    scope.$apply(attrs.howtoload);
                });
            }
        }
    });