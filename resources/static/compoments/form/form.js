angular.module('UserInfoModule',[])

    .controller('UserInfoCtrl',['$scope',function ($scope) {
        $scope.userInfo={
            email:'12345678@qq.com',
            password:'123456',
            autoLogin:true
        }

        $scope.getFormData = function () {
            console.log($scope.userInfo);
        };

        $scope.setFormData = function () {
            $scope.userInfo={
                email:'87654321@qq.com',
                password:'87654321',
                autoLogin:false
            }
        };

        $scope.resetFormData = function () {
            $scope.userInfo={
                email:'12345678@qq.com',
                password:'123456',
                autoLogin:true
            }
        };

    }])