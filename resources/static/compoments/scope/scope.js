angular.module('MyModule',[])

    .controller('MyCtrl',['$scope',function ($scope) {
        $scope.ctrlFlavor = '百威';
    }])

    .directive('drink',function () {
        return{
            restrict:'AE',
            scope:{
                flavor:'@'
            },
            template:'<div>{{flavor}}</div>'
        }
    })

    .controller('MyCtrl2',['$scope',function ($scope) {
        $scope.ctrlFlavor = '百威';
    }])

    .directive('drink2',function () {
        return{
            restrict:'AE',
            scope:{
                flavor:'=',
            },
            template:'<input type="text" ng-model="flavor"/>'
        }
    })

    .controller('MyCtrl3',['$scope',function ($scope) {
        $scope.sayHello = function (name) {
            alert('hello'+name);
        }
    }])

    .directive('greeting',function () {
        return{
            restrict:'AE',
            scope:{
                greet:'&'
            },
            template:'<input type="text" ng-model="userName" /><br>'+
                '<button class="btn btn-default" ng-click="greet({name:userName})">Greeting</button><br>'
        }
    })